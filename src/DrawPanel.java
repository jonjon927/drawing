/* John VanSickle DrawPanel Class
 * This is a class for the drawing panel that will be placed in the DrawFrame
 * has all necessary listeners and fields for drawing objects based on input from DrawFrame UI pieces 
 * 
 */

import java.util.ArrayList;

import javax.swing.*;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

public class DrawPanel extends JPanel {
	
	private ArrayList<Shape> shapes;
	private enum ShapeType{LINE, OVAL, RECTANGLE, TRIANGLE};
	private ShapeType shapeSelected;
	private Shape currentShape;
	private Paint currentColor;
	private Paint previousColor;
	private Color gradientColorOne;
	private Color gradientColorTwo;
	private boolean filledShape;
	private JLabel statusLabel = new JLabel();
	private Stroke currentStroke = new BasicStroke();
	
	public DrawPanel(){
		this.setBackground(Color.WHITE);
		
		//initialize the ArrayList
		shapes = new ArrayList<Shape>();
		//initialize the ShapeType
		shapeSelected = ShapeType.LINE;
		//initialize the color
		currentColor = Color.BLUE;
		//initialize filledShape to false as to match the checkbox
		filledShape = false;
		
		// add the mouse listeners (panel does the listening)
		MouseHandler mouseListener = new MouseHandler();
		this.addMouseListener(mouseListener);
		this.addMouseMotionListener(mouseListener);
	}

	public void paintComponent(Graphics g){
		//draw all of the lines in the ArrayList
		Graphics2D g2d = (Graphics2D) g;
		
		super.paintComponent(g);
		
		for (int i=0; i<shapes.size(); i++){
			shapes.get(i).draw(g2d);
		}
		
		if(currentShape != null)
			currentShape.draw(g2d);
	}
	
	//function to clear the drawpanel
	public void clearThePanel(){
		shapes.clear();
		repaint();
	}
	
	//function to undo (remove) the last added object
	public void undoLastShape(){
		if (shapes.size() != 0)
			shapes.remove(shapes.size() - 1);
		repaint();
	}
	
	//mutator method for currentColor
	public void setCurrentColor(Color pColor){
		currentColor = pColor;
	}
	
	//mutator method for filledCheckBox
	public void setFilled(){
		if(filledShape)
			filledShape = false;
		else
			filledShape = true;
	}
	
	public void setGradientColorOne(Color pColor){
		gradientColorOne = pColor;
	}
	public Color getGradientColorOne(){
		return gradientColorOne;
	}
	
	public void setGradientColorTwo(Color pColor){
		gradientColorTwo = pColor;
	}
	public Color getGradientColorTwo(){
		return gradientColorTwo;
	}
	
	public void setGradientPaint(){
		previousColor = currentColor;
		currentColor = new GradientPaint( 0, 0, gradientColorOne, 50, 50, gradientColorTwo, true );
	}
		
	public void setCurrentStroke(int pWidth){
		currentStroke = new BasicStroke(pWidth);
	}
	public void setCurrentStroke(int pWidth, int pDashLength){
		currentStroke = new BasicStroke(pWidth, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, pDashLength, new float[]{pDashLength}, pDashLength);
	}
	public Stroke getCurrentStroke(){
		return currentStroke;
	}
	
	//mutator for shapeSelected
	public void setShapeSelected(int pNum){
		switch(pNum){
		case 1:
			shapeSelected = ShapeType.LINE;
			break;
		case 2:
			shapeSelected = ShapeType.OVAL;
			break;
		case 3:
			shapeSelected = ShapeType.RECTANGLE;
			break;
		case 4:
			shapeSelected = ShapeType.TRIANGLE;
			break;
		default:
			break;
		}
	}
	
	
	//inner class to handle all mouse events
	private class MouseHandler implements MouseListener, MouseMotionListener{
		public void mousePressed(MouseEvent event){
			switch (shapeSelected){
			case LINE:
				currentShape = new Line(event.getX(), event.getY(), event.getX(), event.getY(), currentColor, currentStroke);
				break;
			case RECTANGLE:
				currentShape = new Rect(event.getX(), event.getY(), event.getX(), event.getY(), currentColor, filledShape, currentStroke);
				break;
			case OVAL:
				currentShape = new Oval(event.getX(), event.getY(), event.getX(), event.getY(), currentColor, filledShape, currentStroke);
				break;
			case TRIANGLE:
				currentShape = new Triangle(event.getX(), event.getY(), event.getX(), event.getY(), currentColor, filledShape, currentStroke);
				break;
			default:
				break;
			}
			
		}
		//overridden method to finish drawing the shape and place it into the array
		public void mouseReleased(MouseEvent event){
			if (currentShape != null){
				currentShape.setX2(event.getX());
				currentShape.setY2(event.getY());
				
				//add my line to the ArrayList
				shapes.add(currentShape);
				currentShape = null;
				repaint();
			}
		}
		//Updates mouse coordinates when dragging to actively draw shapes.
		public void mouseDragged (MouseEvent event){
			if (currentShape != null){
				currentShape.setX2(event.getX());
				currentShape.setY2(event.getY());
				
				statusLabel.setText("(" + event.getX() + "," + event.getY() + ")");
				
				//only repaint so that is constantly updates
				repaint();
			}
		}
		
		//Updates coordinates on the statusLabel of current mouse position. (Does not work when dragged)
		public void mouseMoved (MouseEvent event){
			statusLabel.setText("(" + event.getX() + "," + event.getY() + ")");
		}
		
		@Override
		public void mouseClicked(MouseEvent arg0) {
			//unused abstract class
		}
		@Override
		public void mouseEntered(MouseEvent arg0) {
			//unused abstract class
			
		}
		@Override
		public void mouseExited(MouseEvent arg0) {
			//unused abstract class
			
		}
	}
	
	public JLabel getStatusLabel() {
		return statusLabel;
	}


	public void setStatusLabel(JLabel statusLabel) {
		this.statusLabel = statusLabel;
	}
}
