/* John VanSickle DrawFrame class
 * This class is the highest class and contains main.
 * The frame contains buttons, comboboxes, a checkbox, a label, and a DrawPanel object
 * started incredibly messy code, continuously improving
 */

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputMethodEvent;
import java.awt.event.InputMethodListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.*;


public class DrawFrame extends JFrame {
	private JButton undoButton = new JButton("Undo");
	private JButton clearButton = new JButton("Clear");
	private JComboBox colorSelectionDropdown;
	private JComboBox shapeSelectionDropdown;
	private JCheckBox filledCheckBox = new JCheckBox("Filled");
	private JCheckBox gradientCheckBox = new JCheckBox("Gradient");
	private JButton firstColorGradientButton = new JButton("Color 1");
	private JButton secondColorGradientButton = new JButton("Color 2");
	private JCheckBox dashedCheckBox = new JCheckBox("Dashed Line");
	private JTextField strokeWidthTextF = new JTextField("1");
	private JTextField strokeDashTextF = new JTextField("1");
	private JLabel statusLabel;
	private JPanel buttonPanel = new JPanel();
	private JPanel statusPanel = new JPanel();
	private DrawPanel myDrawPanel;
	//for adding event listeners to buttons, will decide which was clicked in the listener
	private ButtonAL bAL = new ButtonAL(); 
	private filledCheckBoxListener cbl = new filledCheckBoxListener();
	
	//constructor
	public DrawFrame(){
		//initialize the DrawFrame
		super("Java Drawings - John VanSickle");
		this.setLayout(new BorderLayout());
		this.setSize(800, 720);
		
		myDrawPanel = new DrawPanel();
		
		//initialize all the UI pieces
		String[] availableColors = {"Blue", "Red", "Green", "Black", "Cyan", "Dark Grey", "Grey", "Light Grey", "Magenta", "Orange", "Pink", "Yellow", "White"};
		colorSelectionDropdown = new JComboBox(availableColors);
		
		String[] availableShapes = {"Line", "Oval", "Rectangle", "Triangle"};
		shapeSelectionDropdown = new JComboBox(availableShapes);
		
		//add event listeners to buttons
		undoButton.addActionListener(bAL);
		clearButton.addActionListener(bAL);
		firstColorGradientButton.addActionListener(bAL);
		secondColorGradientButton.addActionListener(bAL);
		//add event listeners to combo boxes
		colorSelectionDropdown.addItemListener(new comboColorBoxListener());
		shapeSelectionDropdown.addItemListener(new ShapeComboBoxListener());
		//add event listener to checkboc
		filledCheckBox.addItemListener(cbl);
		gradientCheckBox.addItemListener(cbl);
		dashedCheckBox.addItemListener(cbl);
		gradientCheckBox.setEnabled(false);
		//add item listenjer to the textboxes
		strokeWidthTextF.addActionListener(new TextBoxListener());
		strokeDashTextF.addActionListener(new TextBoxListener());
		
		statusLabel = myDrawPanel.getStatusLabel();
		
		//add pieces to the frame
		buttonPanel.add(undoButton);
		buttonPanel.add(clearButton);
		buttonPanel.add(colorSelectionDropdown);
		buttonPanel.add(shapeSelectionDropdown);
		buttonPanel.add(filledCheckBox);
		buttonPanel.add(gradientCheckBox);
		buttonPanel.add(firstColorGradientButton);
		buttonPanel.add(secondColorGradientButton);
		buttonPanel.add(strokeWidthTextF);
		buttonPanel.add(dashedCheckBox);
		buttonPanel.add(strokeDashTextF);
		statusPanel.add(statusLabel);
		this.add(myDrawPanel,BorderLayout.CENTER);
		this.add(buttonPanel, BorderLayout.NORTH);
		this.add(statusPanel, BorderLayout.SOUTH);
		
		this.setVisible(true);
	}
	
	//inner class to implement the action listener for the buttons
	private class ButtonAL implements ActionListener{
		Color cc;
		JColorChooser colorChooser = new JColorChooser();
		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource()==undoButton){
				myDrawPanel.undoLastShape();
			}
			else if (e.getSource()==clearButton){
				myDrawPanel.clearThePanel();
			}
			else if (e.getSource()==firstColorGradientButton){
				JOptionPane.showMessageDialog(null, colorChooser);
				cc = colorChooser.getColor();
				myDrawPanel.setGradientColorOne(cc);
			}
			else if (e.getSource()==secondColorGradientButton){
				JOptionPane.showMessageDialog(null, colorChooser);
				cc = colorChooser.getColor();
				myDrawPanel.setGradientColorTwo(cc);
				gradientCheckBox.setEnabled(true);
			}
		}
		
	}
	
	//inner class to implement action listener to the color combo box
	//look above for changes to color options, but should be (in order) blue, red, green
	private class comboColorBoxListener implements ItemListener{
		@Override
		public void itemStateChanged(ItemEvent e) {
			switch (e.getItem().toString()){
			case "Blue":
				myDrawPanel.setCurrentColor(Color.BLUE);
				break;
			case "Red":
				myDrawPanel.setCurrentColor(Color.RED);
				break;
			case "Green":
				myDrawPanel.setCurrentColor(Color.GREEN);
				break;
			case "Black":
				myDrawPanel.setCurrentColor(Color.BLACK);
				break;
			case "Cyan":
				myDrawPanel.setCurrentColor(Color.CYAN);
				break;
			case "Dark Grey":
				myDrawPanel.setCurrentColor(Color.DARK_GRAY);
				break;
			case "Grey":
				myDrawPanel.setCurrentColor(Color.GRAY);
				break;
			case "Light Grey":
				myDrawPanel.setCurrentColor(Color.LIGHT_GRAY);
				break;
			case "Magenta":
				myDrawPanel.setCurrentColor(Color.MAGENTA);
				break;
			case "Orange":
				myDrawPanel.setCurrentColor(Color.ORANGE);
				break;
			case "Pink":
				myDrawPanel.setCurrentColor(Color.PINK);
				break;
			case "Yellow":
				myDrawPanel.setCurrentColor(Color.YELLOW);
				break;
			case "White":
				myDrawPanel.setCurrentColor(Color.WHITE);
				break;
			default:
				break;
			}
		}
	}
	
	//inner class to implement item listener to the checkbox for choosing filled or not filled
	private class filledCheckBoxListener implements ItemListener{
		@Override
		public void itemStateChanged(ItemEvent e) {
			if (e.getSource()==filledCheckBox){
				myDrawPanel.setFilled();
			}
			else if (e.getSource()==gradientCheckBox){
				if (gradientCheckBox.isSelected())
					myDrawPanel.setGradientPaint();
				else{
					switch (colorSelectionDropdown.getSelectedIndex()){
					case 0:
						myDrawPanel.setCurrentColor(Color.BLUE);
						break;
					case 1:
						myDrawPanel.setCurrentColor(Color.RED);
						break;
					case 2:
						myDrawPanel.setCurrentColor(Color.GREEN);
						break;
					case 3:
						myDrawPanel.setCurrentColor(Color.BLACK);
						break;
					case 4:
						myDrawPanel.setCurrentColor(Color.CYAN);
						break;
					case 5:
						myDrawPanel.setCurrentColor(Color.DARK_GRAY);
						break;
					case 6:
						myDrawPanel.setCurrentColor(Color.GRAY);
						break;
					case 7:
						myDrawPanel.setCurrentColor(Color.LIGHT_GRAY);
						break;
					case 8:
						myDrawPanel.setCurrentColor(Color.MAGENTA);
						break;
					case 9:
						myDrawPanel.setCurrentColor(Color.ORANGE);
						break;
					case 10:
						myDrawPanel.setCurrentColor(Color.PINK);
						break;
					case 11:
						myDrawPanel.setCurrentColor(Color.YELLOW);
						break;
					case 12:
						myDrawPanel.setCurrentColor(Color.WHITE);
						break;
					default:
						break;
					}
				}
			}
			else {
				if (!dashedCheckBox.isSelected()){
					myDrawPanel.setCurrentStroke(Integer.parseInt(strokeWidthTextF.getText()));
				} else{
					myDrawPanel.setCurrentStroke(Integer.parseInt(strokeWidthTextF.getText()), Integer.parseInt(strokeDashTextF.getText()));
				}
			}
		}
	}

	//inner class to implement action listener to the shapeselection combobox
	private class ShapeComboBoxListener implements ItemListener{
		@Override
		public void itemStateChanged(ItemEvent e) {
			switch (e.getItem().toString()){
			case "Line":
				myDrawPanel.setShapeSelected(1);
				break;
			case "Oval":
				myDrawPanel.setShapeSelected(2);
				break;
			case "Rectangle":
				myDrawPanel.setShapeSelected(3);
				break;
			case "Triangle":
				myDrawPanel.setShapeSelected(4);
				break;
			default:
				break;
			}
		}
	}

	//set up for use to set the width of line through new BasicStroke(int)
	private class TextBoxListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource()==strokeWidthTextF){
				myDrawPanel.setCurrentStroke(Integer.parseInt(strokeWidthTextF.getText()));
			}
			else{
				if(dashedCheckBox.isSelected()){
					myDrawPanel.setCurrentStroke(Integer.parseInt(strokeWidthTextF.getText()), Integer.parseInt(strokeDashTextF.getText()));
				}
			}
		}
	}
	
	public static void main(String[] args) {
		DrawFrame myDrawFrame = new DrawFrame();
	}
}


