/* John VanSickle Line Class
 * Contains everything needed to draw a line using Graphics2D
 */
// Declaration of class MyLine.
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Stroke;

public class Line extends Shape
{
   // call default superclass constructor
   public Line()
   {
     super();
   } // end MyLine no-argument constructor

   // call superclass constructor passing parameters
   public Line( int x1, int y1, int x2, int y2, Paint color, Stroke pStroke )
   {
      super( x1, y1, x2, y2, color, pStroke );
   } // end MyLine constructor

   // draw line in specified color
   public void draw( Graphics2D g )
   {
      g.setPaint(getColor());
      g.setStroke(getStroke());
      
	  g.drawLine(getX1(), getY1(), getX2(), getY2());
   } // end method draw
} // end class MyLine


