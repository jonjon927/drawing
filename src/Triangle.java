/* John VanSickle Triangle Class April 3, 2014
 * This class extends BoundedShape and takes what I designed for an algorithm to draw an isoscoles triangle
 * Much more simple than Prof. Strausser's layout for a triangle.. 
 * Still works keeping polymorphism in mind to be able to still change the stroke and gradient/paint from the control frame 
 */

import java.awt.*;

public class Triangle extends BoundedShape
{
	//Static final variable that signifies the KEY points of
	//the a Triangle
	public static final int POINTS = 3;
	
	//Default constructor for the Triangle class
	public Triangle( )
	{
		//Calling the parent class's constructor
		//so that the appropriate instances of 
		//the higher classes have been properly
		//set
		super();
	}
	
	//Constructor for the Triangle class
	// x1 = the top left and bottom left x coordinate of the shape's bounding box
	// x2 the top right and bottom right x coordinate of the shape's bounding box
	public Triangle( int x1, int y1, int x2, int y2, Paint color, boolean isFilled, Stroke pStroke )
	{
		//Calling the parent class's constructor
		//so that the appropriate instances of 
		//the higher classes have been properly
		//set
		super( x1, y1, x2, y2, color, isFilled, pStroke );
	}
	
	//Method to "draw" the Triangle object
	// g = the graphics to draw the triangle to, and how to draw it
	public void draw( Graphics2D g )
	{
		//do it my way, Strausser comments below
			g.setPaint(getColor());
	      g.setStroke(getStroke());
		 if (isFilled()){
			 g.fillPolygon(GetXPoints(), GetYPoints(), GetNPoints());
		 }
		 else{
			 g.drawPolygon(GetXPoints(), GetYPoints(), GetNPoints());
		 }
	}
	
	//Gets the number of KEY points that make up the Triangle
	public int GetNPoints( )
	{
		return POINTS;
	}

	//Gets the Y coordinates of the key points
	public int[] GetYPoints( )
	{
		int[] yPoints = new int[Triangle.POINTS];
		yPoints[0] = getY1();
		yPoints[1] = getY2();
		 //ensures that the bottem two corners are at same Y value
		yPoints[2] = yPoints[1];
		return yPoints;
	}
	
	//Gets the X coordinates of the key points
	public int[] GetXPoints( )
	{
		int[] xPoints = new int[Triangle.POINTS];
		xPoints[0] = getX1();
		xPoints[1] = getX2();
		//formula so that no matter where initially clicked the second lower point is found correctly
		xPoints[2] = getX2()-2*(getX2()-xPoints[0]); 
		return xPoints;
	}
}
