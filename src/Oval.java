/* John VanSickle Line Class
 * Contains everything needed to draw an Oval using Graphics2D
 */

// Declaration of class MyOval.
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Stroke;

public class Oval extends BoundedShape
{
   // call default superclass constructor
   public Oval()
   {
     super();
   } // end MyOval no-argument constructor

   // call superclass constructor passing parameters
   public Oval( int x1, int y1, int x2, int y2,
      Paint color, boolean isFilled, Stroke pStroke )
   {
     super( x1, y1, x2, y2, color, isFilled, pStroke );
   } // end MyOval constructor

   // draw oval
   public void draw( Graphics2D g )
   {
	      g.setPaint(getColor());
	      g.setStroke(getStroke());
      
      //seems to need an if statement to see whether or not should be filled. No overload?
      if(isFilled())
    	  g.fillOval(getUpperLeftX(), getUpperLeftY(), getWidth(), getHeight());
      else
    	  g.drawOval(getUpperLeftX(), getUpperLeftY(), getWidth(), getHeight());
   } // end method draw
} // end class MyOval


