/* John VanSickle Line Class
 * Contains everything needed to draw a rectangle using Graphics2D
 */

// Declaration of class MyRect.
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Stroke;

public class Rect extends BoundedShape
{
   // call default superclass constructor
   public Rect()
   {
     super();
   } // end MyRect no-argument constructor

   // call superclass constructor passing parameters
   public Rect( int x1, int y1, int x2, int y2,
      Paint color, boolean isFilled, Stroke pStroke )
   {
	   super( x1, y1, x2, y2, color, isFilled, pStroke );
   } // end MyRect constructor

   // draw rectangle
   public void draw( Graphics2D g )
   {
	      g.setPaint(getColor());
	      g.setStroke(getStroke());
	 if (isFilled()){
		 g.fillRect(getUpperLeftX(), getUpperLeftY(), getWidth(), getHeight());
	 }
	 else{
		 g.drawRect(getUpperLeftX(), getUpperLeftY(), getWidth(), getHeight());
	 }
   } // end method draw
} // end class MyRect


